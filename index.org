# Created 2020-05-01 Fri 15:29
#+TITLE: docker-emacs-ci
#+property: header-args  :var projectname=(if nil (symbol-value 'title) "docker-emacs-ci") :exports both :dir (expand-file-name (substitute-in-file-name "~/placemarks/myCodjex/docker--emacs")) :eval never-export :mkdirp t :cache no

#+property: pdir ~/placemarks/myCodjex/

*  docker-emacs
  :PROPERTIES:
  :ID:       da7253d4-ccb2-4e42-9faf-c8c7a260fada
  :END:
** scope

tangle and weave org files and compile

How to use this image
assuming you have an org file and a publish.el file

~$ docker run -it registry.gitlab.com/rosenstrauch/docker-emacs-ci emacs --batch -f toggle-debug-on-error --no-init-file --load publish.el --funcall org-publish-all~

Environment Variables

=EMACS_PACKAGES=

This variable specifies space separated list of packages.
They are installed automatically when the container is executed.

** local setup
#+begin_src shell
mkdir -p purple_dogfood_worclones
touch .mrconfig
git clone git@gitlab.com:rosenstrauch/docker-emacs-cli.git docker-emacs-dart
mr register docker-emacs-dart
#+end_src


might want to setup local vars for e.g org tagle sync
#+begin_example src elisp
((nil . ((org-tanglesync-watch-files . '("index.org"))
         )))

#+end_example

** code
*** dockerfile
#+begin_src dockerfile :tangle  "tangles/Dockerfile"  :eval never-export :mkdirp t
FROM ubuntu:21.04 as final
ARG BRANCH
ARG COMMIT
ARG DATE
ARG URL
ARG VERSION

MAINTAINER rosenstrauch

LABEL org.label-schema.schema-version="1.0" \
org.label-schema.build-date=$DATE \
org.label-schema.vendor="Rosenstrauch" \
org.label-schema.name="rosenstrauch/docker-dart--emacs" \
org.label-schema.description="/docker image for emacs in ci." \
org.label-schema.url="https://registry.gitlab.com/rosenstrauch/docker-emacs-ci" \
org.label-schema.docker.cmd="docker run -v $PWD/:// -v $PWD/css:/css/ --init -it registry.gitlab.com/rosenstrauch/docker-emacs-ci:latest" \
org.label-schema.version="$VERSION" \
org.label-schema.vcs-url=$URL \
org.label-schema.vcs-branch=$BRANCH \
org.label-schema.vcs-ref=$COMMIT

RUN apt-get update && apt-get install -y \
    emacs-nox git plantuml \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /root


COPY init.el /root/.emacs.d/

# setup an entrypoint script
COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
RUN ln -s /usr/local/bin/entrypoint.sh / # backwards compat
ENTRYPOINT ["sh", "/entrypoint.sh"]

CMD [ "emacs" ]
#+end_src

*** entrypoint
#+begin_src shell :tangle  "tangles/entrypoint.sh"  :eval never-export
#! /bin/sh

EMACS_DIR=$HOME/.emacs.d/
EMACS_ELPA_DIR=$EMACS_DIR/elpa/
EMACS_INIT_FILE=$EMACS_DIR/init.el

if [ "$1" = emacs -a ! -d "$EMACS_ELPA_DIR" -a -n "$EMACS_PACKAGES" ]; then
	cat >/tmp/melpa.el <<EOF
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
EOF
	emacs -batch -l /tmp/melpa.el -f package-refresh-contents
	rm -f /tmp/melpa.el
fi

exec "$@"
#+end_src

*** init.el
#+begin_src elisp :tangle "tangles/init.el" :eval never-export
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)

(let ((packages (getenv "EMACS_PACKAGES")))
  (when packages
    (dolist (package (split-string packages))
      (add-to-list 'package-selected-packages (intern package)))))

(package-install-selected-packages)
#+end_src

** devops
** ci
#+begin_src yaml :tangle .gitlab-ci.yml :eval never-export
image: docker:20.10


stages:
  - version
  - build
  - publish


services:
  - docker:20.10-dind

variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"


version:
  stage: version
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
    - release next-version --allow-current > .next-version
  artifacts:
    paths:
    - .next-version
  except:
    - tags
  only:
    - main


build:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  script:
    - echo "RELEASE_URL=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$CI_JOB_ID/artifacts/release" > build_info
    - echo "RELEASE_DESC=\"$(uname -mo) binary\"" >> build_info
    - echo "RELEASE_VERSION=$(<.next-version)" >> build_info
    - echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest tangles
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest

  artifacts:
    paths:
    - build_info

release:
  stage: publish
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
  - rm -f release_info
  - mv build_info release_info
  - source release_info
  - release -v
  - release changelog
  - release commit-and-tag CHANGELOG.md release_info
  - release --ci-commit-tag v$RELEASE_VERSION add-download-link -n release -u $RELEASE_URL -d "$RELEASE_DESC"
  except:
    - tags
  only:
    - main
  artifacts:
    paths:
    - release_info



#+end_src

** remote setup

#+name: create-rosenstrauchs-docker-emacs-dart-remote
#+call: ~/org/codelib/python.org:create-gitlab-project-from-python(projectname="docker-emacs-ci" mytoken=(password-store-get "gitlab/purpleapi") gitlabforgeurl="https://gitlab.com") :results output


#+name: read-purple-dogfood-docker-emacs-dart-remote
#+call: ~/org/codelib/restclient.org:restclient-gitlab-stat-project(name="docker-emacs-ci" mysecretkey=(intern (password-store-get "purpleapi@gitlab")) ) :results value pp :output results
